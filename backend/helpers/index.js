const dayjs = require("dayjs");

module.exports.getDate = (date = "", addDay = 0) => {
  const dataDate = date ? dayjs(date) : dayjs();
  return dayjs(dataDate).add(addDay, "day");
};

module.exports.changeFormat = (date = "", format = "YYYY-MM-DD") => {
  const dataDate = date ? dayjs(date) : dayjs();
  return dayjs(dataDate).format(format);
};

module.exports.diffDate = (startDate, endDate) => {
  return dayjs(endDate).diff(startDate);
};

module.exports.addAmount = (currentAmount, newAmount) => {
  return currentAmount + +(+newAmount > 0 ? newAmount : 0);
};

module.exports.minusAmount = (currentAmount, newAmount) => {
  return currentAmount - +(+newAmount > 0 ? newAmount : 0);
};
