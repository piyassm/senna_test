const { addAmount, minusAmount } = require("../helpers");

describe("helpers", function () {
  test("ExpectData addAmount", function () {
    const respone = addAmount(5, 5);
    const expectData = 10;
    expect(respone).toEqual(expectData);
  });

  test("ExpectData minusAmount", function () {
    const respone = minusAmount(5, 5);
    const expectData = 0;
    expect(respone).toEqual(expectData);
  });
});
