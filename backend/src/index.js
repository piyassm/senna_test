const express = require("express");
const cors = require("cors");
const path = require("path");
const dayjs = require("dayjs");
const {
  getDate,
  changeFormat,
  diffDate,
  addAmount,
  minusAmount,
} = require("../helpers");

const app = express();

const buildPath = path.join(__dirname, "../..", "build");
app.use(express.static(buildPath));

app.use(cors());
app.use(express.json());
const PORT = process.env.PORT || 5000;

let mockData = [];

app.get("/show", async (req, res) => {
  const {
    startDate = dayjs().startOf("month").format("YYYY-MM-DD"),
    endDate = dayjs().endOf("month").format("YYYY-MM-DD"),
  } = req.query;

  const startDateData = new Date(startDate);
  const endDateData = new Date(endDate);
  let mockDataSearch = [...mockData];
  mockDataSearch = mockDataSearch.sort((a, b) => {
    return new Date(a.date) - new Date(b.date);
  });

  mockDataSearch = mockDataSearch.filter((data) => {
    const dataDate = new Date(data.date);
    return dataDate >= startDateData && dataDate <= endDateData;
  });

  let dataB = {};
  let dataI = {};
  let dataE = {};
  let sumBalance = 0;

  const dateData = {
    balance: 0,
  };
  let dateIndex;
  let loopStartDate;
  let addDay = 0;
  do {
    loopStartDate = getDate(startDate, addDay);
    dateIndex = changeFormat(loopStartDate);

    dataI[dateIndex] = { ...dateData };
    dataE[dateIndex] = { ...dateData };

    mockDataSearch
      .filter((dataFilter) => {
        return dataFilter.date == dateIndex;
      })
      .forEach((data) => {
        const dataAmount = data.amount;
        if (data.type === "expense") {
          sumBalance = minusAmount(sumBalance, dataAmount);
          dataE[data.date] = {
            balance: addAmount(dataE[data.date].balance, dataAmount),
          };
        } else {
          sumBalance = addAmount(sumBalance, dataAmount);
          dataI[data.date] = {
            balance: addAmount(dataI[data.date].balance, dataAmount),
          };
        }
        dataB[data.date] = {
          balance: sumBalance,
        };
      });
    dataB[dateIndex] = { balance: sumBalance };

    addDay++;
  } while (diffDate(dateIndex, endDate));

  const response = {
    responseRecord: mockDataSearch,
    responseBalance: dataB,
    responseIncome: dataI,
    responseExpense: dataE,
  };
  res.json(response);
});

app.post("/add", async (req, res) => {
  const { category, type, amount, date } = req.body;
  mockData.push({
    category,
    type,
    amount,
    date,
    created: dayjs().format("YYYY-MM-DD HH:mm:ss"),
  });

  const response = {
    responseRecord: mockData,
  };
  res.json(response);
});

app.post("/reset", async (req, res) => {
  mockData = [];

  const response = {
    responseRecord: mockData,
  };
  res.json(response);
});

app.listen(PORT);

console.log(`listening in port ${PORT}`);
