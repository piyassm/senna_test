Demo Site Here:
```sh
https://protected-garden-01733.herokuapp.com/
```

## Available Scripts

In the project directory

You can run (FRONT END):

### `yarn start-client`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


You can run (BACK END):

### `yarn start-server`

Runs the app in [http://localhost:5000](http://localhost:5000) 

Launches the Unit Test:

### `yarn test`