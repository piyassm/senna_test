import React, { useRef, useEffect } from "react";
import { Chart, registerables } from "chart.js";

let myChart;

const IncomeLineChart = (props) => {
  const chartRef = useRef(null);
  const { balanceLabel, balanceData } = props;

  useEffect(() => {
    if (myChart) {
      myChart.data.labels = balanceLabel;
      myChart.data.datasets[0].data = balanceData;
      myChart.update();
    }
  });

  useEffect(() => {
    Chart.register(...registerables);
    myChart = new Chart(chartRef.current, {
      type: "line",
      data: {
        labels: balanceLabel?.length ? balanceLabel : [],
        datasets: [
          {
            label: "Income Chart",
            data: balanceData?.length ? balanceData : [],
            fill: false,
            borderColor: "#28cb7c",
            backgroundColor: "#28cb7c",
            tension: 0.1,
          },
        ],
      },
    });
  }, []);

  return <canvas ref={chartRef} />;
};

export default IncomeLineChart;
