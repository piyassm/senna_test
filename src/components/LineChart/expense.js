import React, { useRef, useEffect } from "react";
import { Chart, registerables } from "chart.js";

let myChart;

const ExpenseLineChart = (props) => {
  const chartRef = useRef(null);
  const { balanceLabel, balanceData } = props;

  useEffect(() => {
    if (myChart) {
      myChart.data.labels = balanceLabel;
      myChart.data.datasets[0].data = balanceData;
      myChart.update();
    }
  });

  useEffect(() => {
    Chart.register(...registerables);
    myChart = new Chart(chartRef.current, {
      type: "line",
      data: {
        labels: balanceLabel?.length ? balanceLabel : [],
        datasets: [
          {
            label: "Expense Chart",
            data: balanceData?.length ? balanceData : [],
            fill: false,
            borderColor: "#e74c3c",
            backgroundColor: "#e74c3c",
            tension: 0.1,
          },
        ],
      },
    });
  }, []);

  return <canvas ref={chartRef} />;
};

export default ExpenseLineChart;
