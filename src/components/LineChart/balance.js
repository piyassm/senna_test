import React, { useRef, useEffect } from "react";
import { Chart, registerables } from "chart.js";

let myChart;

const BalanceLineChart = (props) => {
  const chartRef = useRef(null);
  const { balanceLabel, balanceData } = props;

  useEffect(() => {
    if (myChart) {
      myChart.data.labels = balanceLabel;
      myChart.data.datasets[0].data = balanceData;
      myChart.update();
    }
  });

  useEffect(() => {
    Chart.register(...registerables);
    myChart = new Chart(chartRef.current, {
      type: "line",
      data: {
        labels: balanceLabel?.length ? balanceLabel : [],
        datasets: [
          {
            label: "Balance Chart",
            data: balanceData?.length ? balanceData : [],
            fill: true,
            borderColor: "rgb(75, 192, 192)",
            tension: 0.1,
          },
        ],
      },
    });
  }, []);

  return <canvas ref={chartRef} />;
};

export default BalanceLineChart;
