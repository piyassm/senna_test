import styled from "styled-components";
import { useEffect, useState } from "react";
import { Modal, Button, Form, Row, Col, Nav } from "react-bootstrap";
import {
  ExpenseLineChart,
  IncomeLineChart,
  BalanceLineChart,
} from "./components/LineChart";
import { DatePicker } from "antd";
import moment from "moment";
import { serviceAdd, serviceShow, serviceReset } from "./requests";

const { RangePicker } = DatePicker;

const AppWrapped = styled.div`
  margin-top: 64px;
`;

const ContainerReport = styled.div`
  background-color: #fff;
  height: 100%;
  border-radius: 10px;
  flex: ${(props) => (props.full ? "1" : "0.5")};
  margin: 0 8px;
`;

const DatePickerWrapped = styled(DatePicker)`
  width: 100%;
`;

const initFormData = {
  category: "",
  type: "expense",
  amount: "",
  date: "",
};

const initSearchData = {
  startDate: moment().startOf("month").format("YYYY-MM-DD"),
  endDate: moment().endOf("month").format("YYYY-MM-DD"),
};

function App() {
  const [dataTracker, setDataTracker] = useState([]);
  const [dataGraphBalance, setDataGraphBalance] = useState({
    label: [],
    data: [],
  });
  const [dataGraphIncome, setDataGraphIncome] = useState({
    label: [],
    data: [],
  });
  const [dataGraphExpense, setDataGraphExpense] = useState({
    label: [],
    data: [],
  });
  const [show, setShow] = useState(false);
  const [dateSearch, setDateSearch] = useState({ ...initSearchData });

  const [formData, setFormData] = useState({ ...initFormData });

  const handleClose = () => {
    setShow(false);
    setFormData({ ...initFormData });
  };
  const handleShow = () => setShow(true);

  const handleFormData = (name, value) => {
    setFormData((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const fetchData = async () => {
    const response = await serviceShow(
      dateSearch?.startDate,
      dateSearch?.endDate
    );
    const { responseRecord, responseBalance, responseIncome, responseExpense } =
      response;
    setDataTracker(responseRecord);
    function setDataGraph(objData) {
      const labelBalance = Object.keys(objData)?.map((data) => {
        return data;
      });
      const dataBalance = Object.keys(objData)?.map((data) => {
        return objData[data].balance;
      });
      return {
        label: labelBalance,
        data: dataBalance,
      };
    }
    if (responseBalance) {
      const getDataGraph = setDataGraph(responseBalance);
      setDataGraphBalance({
        label: getDataGraph.label,
        data: getDataGraph.data,
      });
    }
    if (responseIncome) {
      const getDataGraph = setDataGraph(responseIncome);
      setDataGraphIncome({
        label: getDataGraph.label,
        data: getDataGraph.data,
      });
    }
    if (responseExpense) {
      const getDataGraph = setDataGraph(responseExpense);
      setDataGraphExpense({
        label: getDataGraph.label,
        data: getDataGraph.data,
      });
    }
  };

  useEffect(() => {
    fetchData();
  }, [dateSearch]);

  const addData = async () => {
    const validStatus = Object.keys(formData).every((data) => {
      return formData?.[data]?.length;
    });
    if (!validStatus) {
      return;
    }
    await serviceAdd(formData);
    handleClose();
    fetchData();
  };

  function onChangeFilterData(dates, dateStrings) {
    setDateSearch({ startDate: dateStrings[0] || initSearchData.startDate, endDate: dateStrings[1]|| initSearchData.endDate });
  }

  const resetData = async () => {
    await serviceReset();
    setDateSearch({ ...initSearchData });
    fetchData();
  };

  return (
    <AppWrapped>
      <div className="container">
        <h1>Budget Tracker</h1>
        <div className="d-flex mb-4 justify-content-between">
          <div>
            <button
              type="button"
              className="btn btn-danger"
              onClick={resetData}
            >
              Reset
            </button>
          </div>
          <div>
            <button
              type="button"
              className="btn btn-success"
              onClick={handleShow}
            >
              Add
            </button>
          </div>
        </div>
        <div className="d-flex mb-4 justify-content-center">
          <div>
            <RangePicker
              ranges={{
                Today: [moment(), moment()],
                "This Month": [
                  moment().startOf("month"),
                  moment().endOf("month"),
                ],
              }}
              onChange={onChangeFilterData}
              value={[moment(dateSearch.startDate), moment(dateSearch.endDate)]}
            />
          </div>
        </div>
        <div>
          <table className="table">
            <thead>
              <tr>
                <th scope="col">No.</th>
                <th scope="col">Date</th>
                <th scope="col">Category</th>
                <th scope="col">Income</th>
                <th scope="col">Expense</th>
              </tr>
            </thead>
            <tbody>
              {dataTracker?.map((data, key) => {
                return (
                  <tr key={`rowData-${key}`}>
                    <td>{key + 1}</td>
                    <td>{data.date}</td>
                    <td>{data.category}</td>
                    <td>{data.type === "income" ? data.amount : ""}</td>
                    <td>{data.type === "expense" ? data.amount : ""}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        <div className="d-flex">
          <ContainerReport>
            <IncomeLineChart
              balanceLabel={dataGraphIncome.label}
              balanceData={dataGraphIncome.data}
            />
          </ContainerReport>
          <ContainerReport>
            <ExpenseLineChart
              balanceLabel={dataGraphExpense.label}
              balanceData={dataGraphExpense.data}
            />
          </ContainerReport>
        </div>
        <div className="d-flex mt-3">
          <ContainerReport full>
            <BalanceLineChart
              balanceLabel={dataGraphBalance.label}
              balanceData={dataGraphBalance.data}
            />
          </ContainerReport>
        </div>
      </div>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header>
          <Modal.Title>Create</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Nav justify variant="tabs" activeKey={formData.type}>
            <Nav.Item>
              <Nav.Link
                eventKey="expense"
                onSelect={(value) => handleFormData("type", value)}
              >
                Expense
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link
                eventKey="income"
                onSelect={(value) => handleFormData("type", value)}
              >
                Income
              </Nav.Link>
            </Nav.Item>
          </Nav>
          <Form className="mt-3">
            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm="2">
                Category:
              </Form.Label>
              <Col sm="10">
                <Form.Control
                  type="text"
                  onChange={(e) => handleFormData("category", e.target.value)}
                />
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm="2">
                Amount:
              </Form.Label>
              <Col sm="10">
                <Form.Control
                  type="text"
                  onChange={(e) => handleFormData("amount", e.target.value)}
                />
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm="2">
                Date:
              </Form.Label>
              <Col sm="10">
                <DatePickerWrapped
                  onChange={(value) =>
                    handleFormData("date", value?.format("YYYY-MM-DD"))
                  }
                  placeholder=""
                />
              </Col>
            </Form.Group>
          </Form>
          <div className="text-center">
            <Button variant="success" onClick={addData}>
              Save
            </Button>
            <Button variant="light" className="ml-3" onClick={handleClose}>
              Cancle
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    </AppWrapped>
  );
}

export default App;
