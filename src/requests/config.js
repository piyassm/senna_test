export const API_CONFIG = {
  MAIN_URL: process.env.REACT_APP_API_BASE_URL,
  CONTENT_TYPE_APP_JSON: "application/json",
};

export const HEADER = {
  CONTENT_TYPE_APP_JSON: {
    "Content-Type": API_CONFIG.CONTENT_TYPE_APP_JSON,
  },
};

export const METHOD = {
  POST: "POST",
  GET: "GET",
};

export const API_REQUEST = {
  ADD: API_CONFIG.MAIN_URL + "/add",
  SHOW: API_CONFIG.MAIN_URL + "/show",
  RESET: API_CONFIG.MAIN_URL + "/reset",
};
