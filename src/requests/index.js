import { HEADER, API_REQUEST } from "./config";
import axios from "axios";

export async function callAPIPost(url, data = {}) {
  try {
    return Promise.all([
      axios({
        url,
        method: "POST",
        headers: HEADER.CONTENT_TYPE_APP_JSON,
        data: JSON.stringify(data),
      }),
    ]);
  } catch (error) {
    return { err: { statusCode: 404 + error } };
  }
}

export async function callAPIGet(url) {
  try {
    return Promise.all([
      axios({
        url,
        method: "GET",
        headers: HEADER.CONTENT_TYPE_APP_JSON,
      }),
    ]);
  } catch (error) {
    return { err: { statusCode: 404 + error } };
  }
}

export const serviceAdd = async (formData) => {
  try {
    let url = encodeURI(API_REQUEST.ADD);
    const [RES_API] = await callAPIPost(url, formData);
    return await RES_API.data;
  } catch (error) {
    return { err: { statusCode: 404, error } };
  }
};

export const serviceShow = async (startDate, endDate) => {
  try {
    let url = encodeURI(API_REQUEST.SHOW);
    if (startDate && endDate) {
      url += `?startDate=${startDate}&endDate=${endDate}`;
    }
    const [RES_API] = await callAPIGet(url);
    return await RES_API.data;
  } catch (error) {
    return { err: { statusCode: 404, error } };
  }
};

export const serviceReset = async () => {
  try {
    let url = encodeURI(API_REQUEST.RESET);
    const [RES_API] = await callAPIPost(url);
    return await RES_API.data;
  } catch (error) {
    return { err: { statusCode: 404, error } };
  }
};
